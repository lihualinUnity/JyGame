﻿
using System.IO;
using UnityEditor;
using UnityEngine;

namespace JyEditor
{

    /// <summary>
    /// 文件信息
    /// </summary>
    public class JyFileInfo
    {
        // 文件名(不带后缀)
        public string Name
        {
            get { return _name; }
        }

        // 文件路径(<Assets/>打头, 包含路径名)
        public string AssetPath
        {
            get { return _assetPath; }
        }

        // 文件GUID，唯一Hash码
        public string Guid
        {
            get { return _guid; }
        }

        // 文件扩展名
        public string Extension
        {
            get { return _extension; }
        }

        // 完整文件名(带后缀)
        public string FullName
        {
            get { return _fullName; }
        }

        // 完整路径
        public string Path
        {
            get { return _path; }
        }

        public JyFileInfo(string Name, string assetPath, string guid)
        {
            _name = Name;
            _assetPath = assetPath;
            _guid = guid;
            _extension = System.IO.Path.GetExtension(_assetPath);
            _path = assetPath;
        }

        public JyFileInfo(string path)
        {
            _path = path;

            int index = _path.IndexOf("Assets/");
            if (index > 0)
            {
                _assetPath = _path.Substring(index);
            }
            else
            {
                _assetPath = path;
            }

            _name = System.IO.Path.GetFileNameWithoutExtension(_path);
            _extension = System.IO.Path.GetExtension(_path);

            if (index > 0)
            {
                _guid = AssetDatabase.AssetPathToGUID(_assetPath);
            }
            else
            {
                _guid = System.DateTime.Now + _name;
            }

            _fullName = _name + _extension;
        }

        private string _path;
        private string _name;
        private string _guid;
        private string _fullName;
        private string _assetPath;
        private string _extension;
    }

}